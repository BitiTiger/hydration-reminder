#!/bin/sh

# String, title of the notification (usually an application name)
NOTIFICATION_TITLE="Hydration Reminder"
# String, text body of the notification (usually the actual content)
NOTIFICATION_TEXT="Take a break and drink some water."
# Integer, number of milliseconds before the notification is dismissed
NOTIFICATION_TIMEOUT=30000

# ensure commands are present
if [ ! $(which notify-send 2>/dev/null) ]; then
    echo "[Error] Could not find notify-send utility. Please install it."
    echo "[Error]     On Arch Linux: pacman -S libnotify"
    exit 1
fi
if [ ! $(which xdg-screensaver 2>/dev/null) ]; then
    echo "[Error] Could not find xdg-screensaver utility. Please install it."
    echo "[Error]     On Arch Linux: pacman -S xdg-utils"
    exit 1
fi

# ensure KDE is running
if [ $(ps aux | grep $USER | grep plasmashell | wc -l) -gt 1 ]; then
    echo "[Info] KDE is running."
else
    echo "[Error] KDE is not running. Refusing to run."
    exit 1
fi

# send notification
notify-send -u normal -a "$NOTIFICATION_TITLE" -t $NOTIFICATION_TIMEOUT "$NOTIFICATION_TEXT"

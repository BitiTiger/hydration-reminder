#!/bin/sh

# do not run as root
if [ $USER == "root" ]; then
    echo "This script must run as a non-root user!"
    exit 1
fi

# check for empty destination
if [ -e /usr/local/bin/hydrate-notify ]; then
    echo "/usr/local/bin/hydrate-notify already exists. Refusing to install."
    exit 1
fi

# mark as executable
chmod +x ./hydrate-notify.sh

# ensure bin exists
sudo mkdir -p /usr/local/bin/

# copy script
sudo cp -v ./hydrate-notify.sh /usr/local/bin/hydrate-notify

# add crontab
cp crontab.txt crontab.tmp
crontab -l >> crontab.tmp
crontab crontab.tmp
rm crontab.tmp

# done
echo "Done!"

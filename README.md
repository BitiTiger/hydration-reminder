# Hydration Reminder

Fixing my bad hydration habits with computer science.

![notification](screenshot.png)

## Dependencies

| Utility         | Arch Package |
| :-------------- | :----------- |
| crontab         | cronie       |
| notify-send     | libnotify    |
| xdg-screensaver | xdg-utils    |
| sed             | sed          |

## Install

**NOTE**: Arch Linux users use [these](#makepkg) instructions.

Pick an install type below. **The single user script should work for most
people.**

Don't forget to enable the cronie daemon with `systemctl enable --now cronie`.

### Single User Script

1. Install [dependencies](#dependencies) mentioned above
2. Clone this repo: `git clone git@gitlab.com:BitiTiger/hydration-reminder.git`
3. Enter project directory: `cd hydration-reminder`
4. Run install script as your non-root user: `./install.sh`

### Multiple Users / Manual

1. Install [dependencies](#dependencies) mentioned above
2. Clone this repo: `git clone git@gitlab.com:BitiTiger/hydration-reminder.git`
3. Enter project directory: `cd hydration-reminder`
4. Copy the `hydrate-notify.sh` script to `/usr/local/bin/hydrate-notify`:
  `cp hydrate-notify /usr/local/bin/hydrate-notify`
5. Add `0 * * * * XDG_RUNTIME_DIR=/run/user/$(id -u)
  /usr/local/bin/hydrate-notify` to each user's crontab

### Makepkg (Arch Linux only)

1. Clone this repo: `git clone git@gitlab.com:BitiTiger/hydration-reminder.git`
2. Enter project directory: `cd hydration-reminder`
3. Run `makepkg -si`
4. Follow directions from pacman message

## Uninstall

### Single User Script

To uninstall, run the `uninstall.sh` script as the same user that installed it.

### Multiple Users / Manual

1. Remove `/usr/local/bin/hydrate-notify` jobs from each user's crontab
2. Delete the `/usr/local/bin/hydrate-notify` script: `rm -f
  /usr/local/bin/hydrate-notify`

### Pacman (Arch Linux only)

If you installed with `makepkg`, you can uninstall the normal way.

1. Run `pacman -R hydration-reminder`
2. Remove `/usr/local/bin/hydrate-notify` jobs from each user's crontab (the
  cron job is only present if you manually enabled it)

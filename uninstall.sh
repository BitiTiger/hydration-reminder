#!/bin/sh

# do not run as root
if [ $USER == "root" ]; then
    echo "This script must run as a non-root user!"
    exit 1
fi

# copy script
sudo rm -f /usr/local/bin/hydrate-notify

# add crontab
crontab -l > crontab.tmp
sed -i "/\/usr\/local\/bin\/hydrate-notify/d" crontab.tmp
crontab crontab.tmp
rm crontab.tmp

# done
echo "Done!"
